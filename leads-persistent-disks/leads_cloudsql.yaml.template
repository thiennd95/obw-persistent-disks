apiVersion: apps/v1
kind: Deployment
metadata:
  name: obw
  labels:
    app: obw
spec:
  replicas: 1
  selector:
    matchLabels:
      app: obw
  template:
    metadata:
      labels:
        app: obw
    spec:
      containers:
        - image: gcr.io/obw-platform/obw-drupal
          name: obw
          env:
          - name: OBW_DB_HOST
            value: 127.0.0.1:3306
          - name: OBW_DB_USER
            valueFrom:
              secretKeyRef:
                name: cloudsql-db-credentials
                key: username
          - name: OBW_DB_PASSWORD
            valueFrom:
              secretKeyRef:
                name: cloudsql-db-credentials
                key: password
          ports:
            - containerPort: 80
              name: obw
          volumeMounts:
            - name: obw-persistent-storage
              mountPath: /var/www/html
        - name: cloudsql-proxy
          image: gcr.io/cloudsql-docker/gce-proxy:1.11
          command: ["/cloud_sql_proxy",
                    "-instances=${INSTANCE_CONNECTION_NAME}=tcp:3306",
                    "-credential_file=/secrets/cloudsql/key.json"]
          securityContext:
            runAsUser: 2  # non-root user
            allowPrivilegeEscalation: false
          volumeMounts:
            - name: cloudsql-instance-credentials
              mountPath: /secrets/cloudsql
              readOnly: true
      volumes:
        - name: obw-persistent-storage
          persistentVolumeClaim:
            claimName: obw-volumeclaim
        - name: cloudsql-instance-credentials
          secret:
            secretName: cloudsql-instance-credentials

